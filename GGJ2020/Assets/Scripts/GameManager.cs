﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	private void Awake()
	{
		if (instance != null)
			Destroy(this);

		instance = this;

		DontDestroyOnLoad(gameObject);

		SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
	}

	private void SceneManagerOnSceneLoaded(Scene arg0, LoadSceneMode arg1)
	{
		Time.timeScale = 1;

		game_flow?.ResetUi();
	}

	private void Start()
	{
		game_flow = GetComponent<GameFlowUI>();
	}

	public void Win()
	{
		game_flow.ShowWin();
	}

	public void Lose()
	{
		game_flow.ShowLose();
	}

	[SerializeField] private GameFlowUI game_flow;

	private static GameManager instance = null;
}