﻿using UnityEngine;

public class Player : MonoBehaviour
{
	#region Properties

	public Item			PickedUpItem		{ get; set; } = null;
	public Interactible NearestInteractible { get; set; } = null;

	public AudioSource  source = null;
	public AudioClip clip = null;

	private SpriteRenderer m_buttonSprite = null;

	bool firstPickUp = true;

	#endregion

	#region Unity Methods
	private void Start()
	{
		source = GetComponent<AudioSource>();
		m_buttonSprite = GetComponentInChildren<SpriteRenderer>();
	}

	private void Update()
	{
		if (!NearestInteractible)
			return;

		if (Input.GetButtonDown("Interact"))
		{
			if (firstPickUp && NearestInteractible.tag != "rock")
			{
				firstPickUp = false;
				FluidLevel.Trigger();
			}
			NearestInteractible.Interact(this);


			m_buttonSprite.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
		}

		else if (Input.GetButtonUp("Interact"))
		{
			NearestInteractible.StopInteract(this);

			m_buttonSprite.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	#endregion
}