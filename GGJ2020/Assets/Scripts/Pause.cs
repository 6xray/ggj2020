﻿using UnityEngine;

public class Pause : MonoBehaviour
{
	public static bool IsActive
	{
		get
		{
			if (!game_flow)
				game_flow = FindObjectOfType<GameFlowUI>();

			return game_flow.IsPaused;
		}
	}

	public void Resume()
	{
		Time.timeScale = 1;

		if (!game_flow)
			game_flow = FindObjectOfType<GameFlowUI>();

		game_flow.TooglePause();
	}

	private static GameFlowUI game_flow;
}
