﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShmupRocket : MonoBehaviour
{
    [SerializeField]
    float _speed = 1f;

    public float shieldLoad;
    public int shieldStack;

    [SerializeField]
    LayerMask _collisionLayer;

    [SerializeField]
    ShmupCamera _cam = null;

    [SerializeField]
    Sprite[] _texturesClean;

    [SerializeField]
    Sprite[] _texturesBroke;
    
    [SerializeField]
    Sprite[] _texturesDestruct;

    Sprite[] _textures;

    [SerializeField]
    int _life;

    SpriteRenderer _sprite;

    BoxCollider2D _collider;

    Rigidbody _body;

    Camera _mainCam;

    AudioSource _audio;

    [SerializeField]
    AudioClip[] _clip;

    bool _renderON = true;

    int flashCount = 0;

    [SerializeField]
    float _shootCD = 0.5f;

    float _lastShoot = 0;

    [SerializeField]
    int _lifeUntilDestruct;

    [SerializeField]
    SpriteRenderer _shield;

    bool _win = false;

    public UnityEvent UIevent; 
    public UnityEvent Lose;
    public UnityEvent Win;
    
    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _mainCam = Camera.main;
        _sprite = GetComponent<SpriteRenderer>();
        _lastShoot = _shootCD;
        _textures = _texturesClean;
        _audio = GetComponent<AudioSource>();
        _shield.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = _mainCam.orthographicSize * 2;
        Bounds bounds = new Bounds(
            _mainCam.transform.position,
            new Vector3(cameraHeight * screenAspect, cameraHeight, 0));

        if (Input.GetAxis("Vertical") > 0.5)
            transform.Translate(new Vector3(0, _speed * Time.deltaTime, 0));

        if (Input.GetAxis("Vertical") < -0.5)
            transform.Translate(new Vector3(0, -_speed * Time.deltaTime, 0));

        if (Input.GetAxis("Horizontal") < -0.5)
            transform.Translate(new Vector3(-_speed * Time.deltaTime, 0, 0));

        if (Input.GetAxis("Horizontal") > 0.5)
            transform.Translate(new Vector3(_speed * Time.deltaTime, 0, 0));

        if (Input.GetButtonDown("Fire1"))
            Shoot();

        transform.Translate(new Vector3(0, _cam._scrollSpeed * Time.deltaTime, 0));
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, bounds.min.x + (_collider.bounds.extents.x), bounds.max.x - (_collider.bounds.extents.x));
        pos.y = Mathf.Clamp(pos.y, bounds.min.y + (_collider.bounds.extents.y), bounds.max.y - _collider.bounds.extents.y);
        transform.position = pos;
        _lastShoot += Time.deltaTime;
        int idx = (int)(Time.time * 5) % 2;
        _sprite.sprite = _textures[idx];

        if (transform.position.y >= -72.5 && !_win)
        {
            _win = true;
            if (Win != null)
                Win.Invoke();
        }
    }

    void Shoot()
    {
        if (shieldStack == 3)
            return;
        shieldLoad += 7;
        if (shieldLoad >= 100f)
        {
            shieldLoad = 0;
            shieldStack += 1;
            _audio.clip = _clip[1];
            _audio.Play();
            if (shieldStack == 1)
                _shield.enabled = true;
            if (UIevent != null)
                UIevent.Invoke();
        }
    }

    public void GetHit()
    {
        if (shieldStack > 0)
        {
            _shield.enabled = false;
            shieldStack -= 1;
            shieldLoad = 0;
            _audio.clip = _clip[2];
            _audio.Play();
            InvokeRepeating("Flash", 0.0f, 0.2f);
            if (UIevent != null)
                UIevent.Invoke();
            _collider.enabled = false;
            return;
        }
        shieldLoad = 0;
        _life--;
        if (_life > 0)
        {
            _audio.clip = _clip[0];
            _audio.Play();
            InvokeRepeating("Flash", 0.0f, 0.2f);
            if (_life > _lifeUntilDestruct)
                _textures = _texturesBroke;
            else
                _textures = _texturesDestruct;
        }
        else
        {
            if (Lose != null)
                Lose.Invoke();
            Destroy(gameObject);
        }
        _collider.enabled = false;
    }

    void Flash()
    {
        _renderON = !_renderON;
        _sprite.enabled = _renderON;
        flashCount++;

        if (flashCount == 10)
        {
            flashCount = 0;
            CancelInvoke();
            _collider.enabled = true;
            if (shieldStack > 0)
                _shield.enabled = true;
        }
    }
}
