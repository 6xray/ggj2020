﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShmupUI : MonoBehaviour
{
    [SerializeField]
    ShmupRocket _rocket;

    Slider _slider;

    AudioSource _source;

    [SerializeField]
    AudioClip[] _clip;

    [SerializeField]
    Image[] _images;
    // Start is called before the first frame update
    void Start()
    {
        _rocket.UIevent.AddListener(DisplayStack);
        _slider = GetComponentInChildren<Slider>();
        _source = GetComponentInChildren<AudioSource>();
        _rocket.Lose.AddListener(Lose);
        _rocket.Win.AddListener(Win);
        for (int i = 0; i < _images.Length; ++i)
            _images[i].enabled = false;
        _source.clip = _clip[2];
        _source.Play();
    }

    // Update is called once per frame
    void Update()
    {
        _slider.value = _rocket.shieldLoad / 100;
    }

    void DisplayStack()
    {
        for (int i = 0; i < _images.Length; ++i)
            _images[i].enabled = false;
        for (int i = 0; i < _rocket.shieldStack; ++i)
            _images[i].enabled = true;
    }

    void Lose()
    {
        _source.clip = _clip[0];
        _source.loop = false;
        _source.Play();
        Invoke("LoadScene", _clip[0].length);
    }

    void LoadScene()
    {
        SceneManager.LoadScene(0);
    }

    void Win()
    {
        _source.clip = _clip[1];
        _source.loop = false;
        _source.Play();
        Invoke("LoadScene", _clip[1].length);
    }
}
