﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShmupAsteroid : MonoBehaviour
{
    Vector3 _direction;

    SpriteRenderer _renderer;

    bool _visible;
    private void Start()
    {
        float angle = Random.Range(-60, 60);

        _renderer = GetComponent<SpriteRenderer>();
        _direction = Quaternion.Euler(0, 0, angle) * Vector3.down;
    }

    private void Update()
    {
        if (_visible)
            transform.Translate(_direction * Time.deltaTime * 0.1f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
            ShmupRocket player = collision.GetComponent<ShmupRocket>();
            player.GetHit();
        }

        if (collision.tag == "ShmupBullet")
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnBecameVisible()
    {
        _visible = true;
    }
}
