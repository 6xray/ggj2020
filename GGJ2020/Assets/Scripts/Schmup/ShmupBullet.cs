﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShmupBullet : MonoBehaviour
{
    [SerializeField]
    float _speed = 5f;

    [SerializeField]
    float _range;

    Vector3 _start;
    // Start is called before the first frame update
    void Start()
    {
        _start = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(_start, transform.position) > _range)
            Destroy(gameObject);
        transform.Translate(new Vector3(0, _speed * Time.deltaTime, 0));
    }
}
