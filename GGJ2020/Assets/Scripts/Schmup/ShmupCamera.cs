﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShmupCamera : MonoBehaviour
{
    [SerializeField]
    public float _scrollSpeed;

    Camera _cam;
    // Start is called before the first frame update
    void Start()
    {
        _cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        _cam.transform.Translate(new Vector3(0, _scrollSpeed * Time.deltaTime, 0));
    }
}
