﻿using UnityEngine;

public class BlocableFlow : MonoBehaviour
{
	[SerializeField] private GameObject lake;
	[SerializeField] private GameObject center;

	private int count = 0;
	private bool blocked;
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Interactible"))
		{
			count++;

			if (!blocked && count > 1)
			{
				lake.transform.position += Vector3.down * 5;
				blocked = true;

				transform.position = center.transform.position + (other.gameObject.transform.position - center.transform.position) / 2 + Vector3.forward / 2 + Vector3.down / 2;
			}
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Interactible"))
		{
			count--;

			if (blocked && count < 2)
			{
				lake.transform.position += Vector3.up;
				blocked = false;
			}
		}
	}
}
