﻿using System;
using System.Collections;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
	#region Unity methods

	private void Awake()
	{
		base_position = transform.position;

		SetTargetPosition();
	}

	private void Update()
	{
		transform.position = Vector3.MoveTowards(transform.position, target_pos, (amplitude / travel_time) * Time.deltaTime);
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawLine(transform.position, transform.position + direction.normalized * amplitude);
	}

	#endregion

	#region Methods

	private void SetTargetPosition()
	{
		switch (destination)
		{
			case Destination.None:
				destination = Destination.LastPos;
				target_pos = base_position + direction.normalized * amplitude;
				break;
			case Destination.FirstPos:
				destination = Destination.LastPos;
				target_pos = base_position + direction.normalized * amplitude;
				break;
			case Destination.LastPos:
				destination = Destination.FirstPos;
				target_pos = base_position;
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}

		StartCoroutine(Wait());
	}

	private IEnumerator Wait()
	{
		yield return new WaitForSeconds(travel_time);

		yield return new WaitForSeconds(waiting_time);

		Invoke("SetTargetPosition", 0);
	}

	#endregion

	#region Enums

	private enum Destination
	{
		None,
		FirstPos,
		LastPos,
	}

	#endregion

	#region Properties

	[SerializeField] private float travel_time = 5.0f;
    [SerializeField] private float amplitude = 5.0f;
    [SerializeField] private float waiting_time = 2.0f;
    [SerializeField] private Vector3 direction;

	private Vector3 base_position = Vector3.zero;
	private Vector3 target_pos;
	private Destination destination;

	#endregion
}
