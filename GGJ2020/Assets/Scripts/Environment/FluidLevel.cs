﻿using UnityEngine;

public class FluidLevel : MonoBehaviour
{
	#region Unity methods
	private void Update()
	{
		if (is_rising)
			transform.position += Vector3.up * (max_height / rising_time) * Time.deltaTime;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
			Lose();
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawLine(transform.position, transform.position + Vector3.up * max_height);
	}

	#endregion

	#region Exposed methods

	public void Lose()
	{
		source.clip = clip;
		source.loop = false;
		source.Play();
		GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().Lose();
	}

	public static void Trigger()
	{
		is_rising = true;
	}

	#endregion

	#region Members

	[SerializeField] private float max_height = 5;
	[SerializeField] private float rising_time = 60;
	[SerializeField] private AudioSource source;
	[SerializeField] private AudioClip clip;

	private static bool is_rising;

	#endregion
}
