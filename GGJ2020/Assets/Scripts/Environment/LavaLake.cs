﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaLake : MonoBehaviour
{
    private void Start()
    {
	    lava_lake = FindObjectOfType<FluidLevel>();
    }

    private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
			lava_lake.Lose();
	}

    private FluidLevel lava_lake;
}
