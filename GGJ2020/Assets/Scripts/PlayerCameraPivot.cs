﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraPivot : MonoBehaviour
{
	[SerializeField]
	private Transform m_target = null;

	[SerializeField]
	private float m_rotationSpeed = 180f;

	private float m_rotationInput = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		m_rotationInput = Input.GetAxis("CameraRotation");
		transform.position = m_target.transform.position;
    }

	void FixedUpdate()
	{
		if (m_target == null)
			return;

		transform.position = m_target.position;//Vector3.Slerp(transform.position, m_target.transform.position, Time.fixedDeltaTime * 20);

		if (m_rotationInput != 0f)
		{
			Quaternion deltaRotation = Quaternion.AngleAxis(m_rotationInput * m_rotationSpeed * Time.fixedDeltaTime, Vector3.up);
			transform.rotation *= deltaRotation;
		}
	}
}
