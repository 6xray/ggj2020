﻿using System;

using UnityEngine;

public class MovementHandler : MonoBehaviour
{
	#region Variables

	[SerializeField]
	private float		m_moveSpeed				= 5.0f;
	[SerializeField]
	private float		m_moveSpeedWhileJumping = 2.0f;
	[SerializeField]
	private float		m_jumpImpulseWhenMoving	= 5.0f;
	[SerializeField]
	private float		m_jumpImpulseWhenStatic = 5.0f;
	[SerializeField]
	private float		m_jumpPitch				= 45f;
	[SerializeField]
	private Transform	m_bodyGFX				= null;
	[SerializeField]
	private float		m_slerpCoefficient		= 3.0f;

	private Camera				m_mainCamera			= null;
	private Rigidbody			m_rigidbody				= null;
	private CapsuleCollider		m_collider				= null;
	private Animator			m_animator				= null;
	private IsGroundedCollider	m_isGroundedCollider	= null;

	#endregion

	#region Unity Methods

	private void Awake()
	{
		m_mainCamera = Camera.main;
		m_collider = GetComponent<CapsuleCollider>();
		m_rigidbody = GetComponent<Rigidbody>();
		m_animator = GetComponentInChildren<Animator>();
		m_isGroundedCollider = GetComponentInChildren<IsGroundedCollider>();
	}

	private void Update()
	{
		bool    isGrounded   = IsGrounded();
		float   currentSpeed = 0.0f;

		Vector3 cameraDirection = Input.GetAxis("Horizontal") * Camera.main.transform.right + Input.GetAxis("Vertical") * Camera.main.transform.forward;
		Vector3 direction = new Vector3(Vector3.Dot(cameraDirection, Vector3.right), 0, Vector3.Dot(cameraDirection, Vector3.forward));

		if (direction != Vector3.zero)
		{
			if (!isGrounded)
				currentSpeed = m_moveSpeedWhileJumping;
			else
				currentSpeed = m_moveSpeed;

			transform.Translate(direction * (currentSpeed * Time.deltaTime));

			m_bodyGFX.forward = Vector3.Slerp(m_bodyGFX.forward, transform.TransformDirection(direction), Time.deltaTime * m_slerpCoefficient);
		}

		m_animator.SetFloat("MoveSpeed", currentSpeed);

		if (Input.GetButtonDown("Jump") && isGrounded)
		{
			if (direction != Vector3.zero)
			{
				Quaternion rot = Quaternion.AngleAxis(m_jumpPitch, Vector3.Cross(direction, Vector3.up));
				Vector3 jumpDirection = rot * direction;
				m_rigidbody.AddForce(jumpDirection.normalized * m_jumpImpulseWhenMoving, ForceMode.Impulse);
			}
			else
				m_rigidbody.AddForce((Vector3.up + transform.TransformDirection(direction)).normalized * m_jumpImpulseWhenStatic, ForceMode.Impulse);

			transform.parent = null;

			m_animator.SetBool("IsJumping", true);
		}

		else
		{
			m_animator.SetBool("IsJumping", false);
		}

		m_animator.SetBool("IsGrounded", isGrounded);
	}

	private void OnCollisionEnter(Collision p_other)
	{
		if (p_other.gameObject.layer != LayerMask.NameToLayer("Ground") || !IsGrounded())
			return;

		Debug.Log("YO");

		transform.parent = p_other.transform;
	}

	#endregion

	#region Local Methods

	private bool IsGrounded()
	{
		return m_isGroundedCollider.IsGrounded;
	}

	#endregion
}