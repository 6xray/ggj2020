﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameFlowUI : MonoBehaviour
{
	#region Unity methods
	private void Start()
    {
        Debug.Assert(win_canvas != null);
        Debug.Assert(lose_canvas != null);
        Debug.Assert(pause_canvas != null);

        event_system = GetComponent<EventSystem>();

		event_system.SetSelectedGameObject(FindObjectOfType<Canvas>()?.transform.Find("StartButton")?.gameObject);

		win_canvas = Instantiate(win_canvas);
        lose_canvas = Instantiate(lose_canvas);
        pause_canvas = Instantiate(pause_canvas);

        DontDestroyOnLoad(win_canvas);
        DontDestroyOnLoad(lose_canvas);
        DontDestroyOnLoad(pause_canvas);

        win_canvas.SetActive(false);
		lose_canvas.SetActive(false);
		pause_canvas.SetActive(false);
	}

	private void Update()
    {
	    if (Input.GetKeyDown("joystick button 7"))
			TooglePause();
    }

	#endregion

	#region Exposed methods

	public void ShowWin()
	{
		win_canvas.SetActive(true);

		event_system.SetSelectedGameObject(win_canvas.transform.Find("Continue").gameObject);

		Time.timeScale = 0;
	}

	public void ShowLose()
	{
		lose_canvas.SetActive(true);

		event_system.SetSelectedGameObject(lose_canvas.transform.Find("Restart").gameObject);

		Time.timeScale = 0;
	}

	public void ResetUi()
	{
		win_canvas.SetActive(false);
		lose_canvas.SetActive(false);
		pause_canvas.SetActive(is_paused = false);

		Time.timeScale = 1;

		if (SceneManager.GetActiveScene().buildIndex != 0)
			event_system?.SetSelectedGameObject(FindObjectOfType<Button>()?.gameObject);
		else
			event_system?.SetSelectedGameObject(GameObject.Find("StartButton"));
	}
	public void TooglePause()
	{
		if (SceneManager.GetActiveScene().buildIndex == 0)
			return;

		pause_canvas.SetActive(is_paused = !is_paused);

		Time.timeScale = is_paused ? 0 : 1;

		event_system?.SetSelectedGameObject(pause_canvas.transform.Find("Continue").gameObject);
	}

	#endregion

	#region Properties

	public bool IsPaused => is_paused;

	#endregion 

	#region Members

	[SerializeField] private GameObject win_canvas;
	[SerializeField] private GameObject lose_canvas;
	[SerializeField] private GameObject pause_canvas;

	private bool is_paused;
	private EventSystem event_system;

	#endregion
}
