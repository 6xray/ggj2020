﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsGroundedCollider : MonoBehaviour
{
	private bool m_isGrounded = false;
	private BoxCollider m_collider = null;

	public bool IsGrounded => m_isGrounded;

	void Awake()
	{
		m_collider = GetComponent<BoxCollider>();
	}

	void Update()
	{
		LayerMask layers = LayerMask.GetMask("Ground");
		Collider[] overlapingColliders = Physics.OverlapBox(transform.TransformPoint(m_collider.center), m_collider.size / 2, transform.rotation, layers);
		if (overlapingColliders.Length != 0)
			m_isGrounded = true;
		else
			m_isGrounded = false;
	}

	private void OnTriggerEnter(Collider p_other)
	{
		if (p_other.gameObject.layer != LayerMask.NameToLayer("Ground") || !m_isGrounded)
			return;

		transform.parent.parent = p_other.transform;
	}
}
