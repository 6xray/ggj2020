﻿using UnityEngine;

public class Item : Interactible
{
	#region Variables

	private Vector3 m_initialScale = Vector3.zero;

	#endregion

	#region Unity Methods

	protected override void Start()
	{
		m_source = GetComponent<AudioSource>();
		m_initialScale = transform.localScale;
	}

	#endregion

	#region Local Methods

	public override void Interact(Player p_player)
	{
		if (p_player.PickedUpItem)
			return;

		p_player.PickedUpItem        = this;
		p_player.NearestInteractible = null;

		p_player.PickedUpItem.gameObject.SetActive(false);

		p_player.source.Play();
	}

	public override void StopInteract(Player p_player)
	{
		return;
	}

	public override bool CanInteract(Player p_player)
	{
		return p_player && !p_player.PickedUpItem;
	}

	#endregion
}