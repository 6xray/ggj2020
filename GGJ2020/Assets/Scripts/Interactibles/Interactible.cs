﻿using UnityEngine;

public abstract class Interactible : MonoBehaviour
{
	protected AudioSource m_source = null;

	protected virtual void Start()
	{
		m_source = GetComponent<AudioSource>();
	}

	protected virtual void OnTriggerEnter(Collider p_collider)
	{
		if (!p_collider.CompareTag("Player"))
			return;

		Player player = p_collider.GetComponent<Player>();

		if (!CanInteract(player))
			return;

		player.NearestInteractible = this;

		SpriteRenderer sprite = p_collider.GetComponentInChildren<SpriteRenderer>();

		if (sprite)
			sprite.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	}

	protected virtual void OnTriggerExit(Collider p_collider)
	{
		if (!p_collider.CompareTag("Player"))
			return;

		Player player = p_collider.GetComponent<Player>();

		if (!player)
			return;

		player.NearestInteractible = null;

		SpriteRenderer sprite = p_collider.GetComponentInChildren<SpriteRenderer>();

		if (sprite)
			sprite.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
	}

	public abstract void Interact		(Player p_player);
	public abstract void StopInteract	(Player p_player);
	public abstract bool CanInteract	(Player p_player);
}