﻿using UnityEngine;

public class Movable : Interactible
{
	protected override void OnTriggerExit(Collider p_collider)
	{
		if (!p_collider.CompareTag("Player"))
			return;

		base.OnTriggerExit(p_collider);

		StopInteract(null);
	}

	public override void Interact(Player p_player)
	{
		GetComponent<Rigidbody>().mass = 1;
	}

	public override void StopInteract(Player p_player)
	{
		GetComponent<Rigidbody>().mass = 1000;
	}

	public override bool CanInteract(Player p_player)
	{
		return p_player && !p_player.NearestInteractible;
	}
}