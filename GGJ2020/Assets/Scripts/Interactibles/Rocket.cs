﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : Interactible
{
	#region Variables

	private GameObject[] m_parts = null;

	[SerializeField]
	AudioSource _music;

	[SerializeField]
	AudioClip _winClip;

	#endregion

	#region Unity Methods

	private void Awake()
	{
		m_parts = new GameObject[transform.childCount];

		for (int i = 0; i < m_parts.Length; ++i)
		{
			m_parts[i] = transform.GetChild(i).gameObject;
		}
	}

	#endregion

	#region Local Methods

	public override void Interact(Player p_player)
	{
		if (!p_player)
			return;

		if (p_player.PickedUpItem)
		{
			foreach (GameObject part in m_parts)
			{
				if (part.name != p_player.PickedUpItem.name)
					continue;

				part.SetActive(true);

				p_player.NearestInteractible = null;
				p_player.PickedUpItem        = null;

				m_source.Play();

				return;
			}
		}

		else
		{
			foreach (GameObject part in m_parts)
			{
				if (part.name == "Battery")
					continue;
				if (!part.activeInHierarchy)
					return;
			}

			_music.clip = _winClip;
			_music.loop = false;
			_music.Play();
			GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().Win();
		}
	}

	public override void StopInteract(Player p_player)
	{
		throw new System.NotImplementedException();
	}

	public override bool CanInteract(Player p_player)
	{
		return p_player;
	}

	public void Load()
	{
		SceneManager.LoadScene(2);
	}

	#endregion
}