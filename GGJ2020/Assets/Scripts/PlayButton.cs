﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour
{
    Button _play;

    [SerializeField]
    AudioSource _source;

    private void Start()
    {
        _play = GetComponentInChildren<Button>();
        _play.onClick.AddListener(Play);
    }

    public void Play()
    {
        Invoke("Load", _source.clip.length);
    }

    void Load()
    {
        SceneManager.LoadScene(1);
    }
}
