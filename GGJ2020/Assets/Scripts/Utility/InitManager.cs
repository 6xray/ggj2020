﻿using UnityEngine;

public class InitManager : MonoBehaviour
{
	private void Awake()
	{
		if (instance == null)
			instance = Instantiate(game_manager);

		Cursor.visible = false;
	}

	[SerializeField] private GameObject game_manager;

	private static GameObject instance = null;
}
