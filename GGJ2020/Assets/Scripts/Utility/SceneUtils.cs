﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneUtils : MonoBehaviour
{
	public void LoadFirstScene()
	{
		SceneManager.LoadScene(0);
	}

	public void ReloadScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void LoadNextScene()
	{
		SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1));
	}

	public void Quit()
	{
		Application.Quit();
	}
}
